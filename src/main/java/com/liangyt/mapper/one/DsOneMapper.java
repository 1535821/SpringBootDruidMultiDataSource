package com.liangyt.mapper.one;

import com.github.pagehelper.Page;
import com.liangyt.entity.datasourceone.DsOne;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
public interface DsOneMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DsOne record);

    DsOne selectByPrimaryKey(Integer id);

    List<DsOne> selectAll();

    int updateByPrimaryKey(DsOne record);

    Page<DsOne> getPage();
}